Angular Exchange Rates Calculator App

Angular Exchange Rates Calculator App

Requirements

1. Get and display exchange courses from external JSON API. Suggestion : http://openapi.ro/#exchange .

You can use the angular $http.jsonp (https://docs.angularjs.org/api/ng/service/$http#jsonp) method in order

to communicate with openapi.ro.

2. Implement an exchange money form similar to the one on http://www.cursbnr.ro/ .

The user should be able to select the from and to currency (from a list of supported currencies), and enter an

amount. The system should display the converted amount at the daily rate.

3. OPTIONAL : Integrate a d3js line chart (http://d3js.org/) and display the exchange rates for a given currency

for the last 7 days.

Restrictions

1. Use the Angular Material framework as base of your components. https://material.angularjs.org/latest/

2. Use the Swiip generator-gulp-angular in order to set up your work environment.

https://github.com/Swiip/generator-gulp-angular

Follow the tutorial in the link above.

3. Apply JavaScript, Angular, CSS best practices.

4. Layout is up to you

Deliverables

1. Complete source code, preferably on a public bitbucket account.

2. OPTIONAL : We can deploy your application on one of our web servers for a demo.