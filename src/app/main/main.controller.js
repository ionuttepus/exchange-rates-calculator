(function() {
  'use strict';

  angular
    .module('ercProject')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($scope, toastr, exchangeData) {

    var currencyTo = null,
      currencyFrom = null;

    $scope.options = {
      chart: {
        type: 'lineChart',
        height: 450,
        margin : {
          top: 20,
          right: 20,
          bottom: 40,
          left: 55
        },
        x: function(d){ return d.x; },
        y: function(d){ return d.y; },
        useInteractiveGuideline: true,
        dispatch: {
          stateChange: function(e){ console.log("stateChange"); },
          changeState: function(e){ console.log("changeState"); },
          tooltipShow: function(e){ console.log("tooltipShow"); },
          tooltipHide: function(e){ console.log("tooltipHide"); }
        },
        xAxis: {
          axisLabel: 'Day'
        },
        yAxis: {
          axisLabel: 'Currency',
          tickFormat: function(d){
            return d3.format('.04f')(d);
          },
          axisLabelDistance: -10
        },
        callback: function(chart){
          console.log("!!! lineChart callback !!!");
        }
      },
      title: {
        enable: true,
        text: 'Display currency for the previous 7 days'
      }
    };

    $scope.data = [];
    $scope.selected = {name:'AED', value: '1.1303'};
    $scope.currency = null;
    $scope.currencies = [];
    $scope.result = "";

    exchangeData.getDataCurrentDay().then(function(response){
      var data = response.data, rate = data.rate, key;
      toastr.info('loading exercises...');

      for(key in rate){
        $scope.currencies.push({
          name: key,
          value: rate[key]
        });
      }
    });


    $scope.check =function(selected, currencies){
      for(var i in currencies){
        if(currencies[i].name == selected.name){
          return currencies[i];
        }
      }
    };

    $scope.setCurrencyFrom = function(currency){
      currencyFrom = currency;
      $scope.tagFrom = currencyFrom.name;
    };

    $scope.setCurrencyTo = function(currency){
      currencyTo = currency;
      $scope.tagTo = currencyTo.name;
    };

    $scope.calculateCurrency = function(event, value){
      var ron = null;
      if(/^[0-9]*$/.test(value) && (currencyTo !== null) && (currencyFrom !== null)){
        ron = value * parseFloat(currencyFrom.value);
        ron = ron / parseFloat(currencyTo.value);
        $scope.result = ron.toFixed(4);
      }
      if(currencyTo == null){
        toastr.error('The currency TO is not selected');
      }
      if(currencyFrom == null){
        toastr.error('The currency FROM is not selected');
      }
      if(!/^[0-9]*$/.test(value)){
        toastr.error('Only numbers are allowed');
      }
    };

    $scope.setCurrencyChart = function(currency){
      var rate = currency.name;
      $scope.data = [];
      exchangeData.getDataPreviousDays(rate.toLowerCase(), function(chartData){
        $scope.data = [{
          key: rate,
          values: chartData
        }];
        if(!$scope.$$phase) {
          $scope.$apply();
        }
      });
    };
  }
})();
