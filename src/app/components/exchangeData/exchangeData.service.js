(function() {
  'use strict';

  angular
    .module('ercProject')
    .factory('exchangeData', exchangeData);

  /** @ngInject */
  function exchangeData($log, $http) {
    var apiHost = 'http://openapi.ro/api/exchange/',
      apiParams = '?date=',
      apiSelect = 'all',
      apiExtension = '.json';//format data: 2011-01-28

    var dateList = [], counterDays = 0, dateListLength = 0, chartData = null;

    var  config = {
      params: {
        action: "query",
        prop: "revisions",
        format: "json",
        rvlimit: 50,
        callback: "JSON_CALLBACK"
      }
    };

    var service = {
      getDataCurrentDay: getDataCurrentDay,
      getDataPreviousDays: getDataPreviousDays
    };

    function dateFormat(date) {
      var yyyy = date.getFullYear().toString();
      var mm = (date.getMonth()+1).toString(); // getMonth() is zero-based
      var dd  = date.getDate().toString();
      return yyyy + "-" + (mm[1]?mm:"0"+mm[0]) + "-" + (dd[1]?dd:"0"+dd[0]); // padding
    }

    function getPreviousDay(dayBefore){
      var today = new Date(),
        yesterday = new Date(today);
      yesterday.setDate(today.getDate() - dayBefore);
      return yesterday;
    }

    function getCurrentDate(){
      return new Date();
    }

    function initDateList(){
      for(var i = 0; i < 7; i++){
        dateList.push(dateFormat(getPreviousDay(i)));
      }
      dateListLength = dateList.length;
    }

    function requestData(rate, date){
      var url = apiHost + rate + apiExtension + apiParams + date;
      return $http.jsonp(url, config);
    }

    function chainingData(currency, callback){
      requestData(currency, dateList[counterDays]).then(function(response){
        var data = response.data, rate = data.rate;
        chartData.push({x: counterDays + 1, y: rate});
        counterDays += 1;
        if(counterDays == dateListLength){
          callback(chartData);
        }else{
          chainingData(currency, callback);
        }
      });
    }

    function getDataPreviousDays(rate, callback){
      counterDays = 0;
      chartData = [];
      return chainingData(rate, callback);
    }

    function getDataCurrentDay() {
      var currentDate = dateFormat(getCurrentDate());
      return requestData(apiSelect, currentDate);
    }

    initDateList();

    return service;
  }
})();
