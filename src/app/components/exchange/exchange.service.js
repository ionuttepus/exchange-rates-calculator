(function() {
    'use strict';

    angular
        .module('ercProject')
        .directive('exchange', exchange);

    function exchange() {
        return {
            scope: {
                item: '=exchange'
            },
            restrict: 'EA',
            template: '<td><h4>{{item.name}}</h4></td><td>{{item.value}}</td>'
        };
    }
})();