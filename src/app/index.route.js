(function() {
  'use strict';

  angular
    .module('ercProject')
    .config(routeConfig);

  function routeConfig($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'mainExchange'
      })
      .otherwise({
        redirectTo: '/'
      });
  }

})();
