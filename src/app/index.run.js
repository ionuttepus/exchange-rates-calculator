(function() {
  'use strict';

  angular
    .module('ercProject')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {
    $log.debug('runBlock end');
  }

})();
