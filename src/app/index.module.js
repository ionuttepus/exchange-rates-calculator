(function() {
  'use strict';

  angular
    .module('ercProject', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngMessages', 'ngAria', 'ngRoute', 'toastr', 'nvd3']);

})();
